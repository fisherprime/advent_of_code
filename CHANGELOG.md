<a name=""></a>
##  (2019-10-19)


#### Features

* ***:**  Add SPDX license identifier ([57baa88d](57baa88d))
* **2018/day_3:**  "Complete" advent of code challenge ([3c1abe2b](3c1abe2b))
* **CHANGELOG.md:**  Add CHANGELOG ([af4ece47](af4ece47))

#### Bug Fixes

* **CHANGELOG.md:**  Refile CHANGELOG ([5476650f](5476650f))
* **repose_record/*/main.rs:**  Update parse function ([b48aba89](b48aba89))
