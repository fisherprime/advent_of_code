// SPDX-License-Identifier: MIT

use std::collections::btree_map::BTreeMap;
use std::io;

// [1518-11-01 00:00] Guard #10 begins shift
// [1518-11-01 00:05] falls asleep
#[derive(Default, Debug)]
struct ReposeRecord {
    cycle: Vec<i32>,
    date: String,
    time: i32,
}

#[derive(Default, Debug)]
struct Guard {
    guard_id: i32,
    num_records: usize,
    repose_records: Vec<ReposeRecord>,
}

fn main() {
    let mut user_input = String::new();

    let mut guard_records = BTreeMap::<i32, Guard>::new();

    println!(
        "Enter repose record(s) \n\
         As a semi-colon separated list: [1518-11-01 00:00] Guard #10 begins shift;..."
    );

    io::stdin()
        .read_line(&mut user_input)
        .expect("Failed to read repose list");

    parse_repose_records(&mut guard_records, &user_input);

    // TODO: add prettification
    for (_, record) in guard_records.into_iter() {
        println!("{:?}", record.repose_records);
    }
}

// #[allow(unused_variables, unused_mut)]
fn parse_repose_records(guard_records: &mut BTreeMap<i32, Guard>, repose_string: &str) {
    let records_vec: Vec<&str> = repose_string.split(';').collect();

    let mut guard_record = &mut Guard {
        guard_id: -1,
        num_records: 0,
        repose_records: vec![],
    };
    let mut r_record = ReposeRecord {
        cycle: vec![0; 60],
        date: String::from(""),
        time: -1,
    };

    let mut vec_iterator = records_vec.into_iter().peekable();

    while let Some(record) = vec_iterator.next() {
        let datetime_dets_split: Vec<&str> = record.split(']').collect();

        // TODO get MM-DD & mm from time
        let datetime_split: Vec<&str> = datetime_dets_split[0].split_whitespace().collect();
        let date_vec: Vec<&str> = datetime_split[0].trim_matches('[').split('-').collect();
        let r_date = format!("{}-{}", date_vec[1], date_vec[2]);

        let time_vec: Vec<&str> = datetime_split[1].split(':').collect();
        let r_time = time_vec[1].parse::<i32>().expect("Time value is invalid");

        if datetime_dets_split[1].contains('#') {
            let dets_split: Vec<&str> = datetime_dets_split[1].split('#').collect();
            let dets_split_1: Vec<&str> = dets_split[1].split_whitespace().collect();

            if dets_split_1.len() != 3 {
                println!("Error: invalid record length");

                continue;
            }

            let guard_id = dets_split_1[0]
                .trim()
                .parse::<i32>()
                .expect("Guard ID is not a number");
            guard_record = guard_records.entry(guard_id).or_default();
            guard_record.guard_id = guard_id;

            if guard_record.num_records == 0 {
                guard_record.repose_records = Vec::new();
            }

            r_record.date = r_date;
            r_record.time = r_time;

            continue;
        }

        if datetime_dets_split[1].contains("falls asleep") && r_record.date.eq(&r_date) {
            r_record.time = r_time;

            match vec_iterator.peek() {
                Some(_) => continue,
                None => {
                    guard_record.repose_records.push(r_record);
                    break;
                }
            }
        } else if datetime_dets_split[1].contains("wakes up") {
            if r_record.date.eq(&r_date) {
                for iter in r_record.time..r_time {
                    r_record.cycle[iter as usize] += 1;
                }

                r_record.time = r_time;

                match vec_iterator.peek() {
                    Some(_) => continue,
                    None => {
                        guard_record.repose_records.push(r_record);
                        break;
                    }
                }
            }

            println!("Guard #{} never woke up", guard_record.guard_id);
        }
    }
}

// [1518-11-01 00:00] Guard #10 begins shift;[1518-11-01 00:05] falls asleep; [1518-11-01 00:25] wakes up

#[cfg(test)]
mod tests {

    #[test]
    fn parse_repose_records_test() {}
}
