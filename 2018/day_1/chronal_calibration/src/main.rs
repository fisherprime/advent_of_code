// SPDX-License-Identifier: MIT

use std::io;
use std::io::Write; // Brings flush into scope

fn main() {
    let initial_freq;
    let mut final_freq;

    let mut freq_changes = Vec::new();
    let mut user_string = String::new();

    // .unwrap() returns the contained value --of a response-- or the default
    print!("[+] Input starting frequency \n: ");
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut user_string)
        .expect("[!] Failed to read initial frequrency");

    if user_string.len() < 2 {
        return;
    }

    // Must trim --remove whitespace-- read_line input
    initial_freq = user_string
        .trim()
        .parse::<i32>()
        .expect("[!] Failed to parse initial frequency");

    user_string.clear();
    print!(
        "[+] Input frequency change(s) \n\
         As a comma delimited string: +1, -1, ... \n\
         : "
    );
    io::stdout().flush().unwrap();
    io::stdin()
        .read_line(&mut user_string)
        .expect("[!] Failed to read frequency change string");

    if user_string.len() < 2 {
        return;
    }

    str_to_vec(&mut freq_changes, &user_string.trim_end());

    final_freq = initial_freq;
    for freq_change in freq_changes {
        final_freq += freq_change;
    }

    println!(
        "\n[+] Initial frequency: {} \n\
         [+] Final frequency: {}",
        initial_freq, final_freq
    );
}

fn str_to_vec(i_vector: &mut Vec<i32>, i_string: &str) {
    let string_vec: Vec<&str> = i_string.split(',').collect();

    for item in string_vec {
        let curr_digit = item.trim();

        match curr_digit.parse::<i32>() {
            Ok(i) => {
                i_vector.push(i);
            }
            Err(_err) => {
                continue;
            }
        }
    }

    println!("Vec {:?}", i_vector);
}

#[cfg(test)]
mod tests {
    use super::*; // Import names from outer scope

    #[test]
    fn string_to_vector_test() {
        let mut test_vec = Vec::new();

        // Convert to String else the string will be of type "str"
        str_to_vec(&mut test_vec, &"+2, -1, -3, +4, -6");
        assert_eq!(vec![2, -1, -3, 4, -6], test_vec);
    }
}
