// SPDX-License-Identifier: MIT

use std::collections::btree_map::BTreeMap;
use std::io;

// Derive some default values
#[derive(Default, Debug)]
struct Claim {
    claim_id: i32,
    left_margin: i32,
    top_margin: i32,
    width: i32,
    height: i32,
}

// const FABRIC_DIMENSION: usize = 1000;
const FABRIC_DIMENSION: usize = 50;

fn main() {
    let mut user_string = String::new();

    let mut claims = BTreeMap::<i32, Claim>::new();

    println!(
        "[+] Input claim(s) \n\
         As a semi-colon separated list: #1 @ 1,3: 4x4; #2 @ 3,1: 4x4; ... \n"
    );
    io::stdin()
        .read_line(&mut user_string)
        .expect("[!] Failed to read claim list");

    populate_claims(&mut claims, user_string.trim());

    // BTreeMap doesn't implement a clone function
    for claim in &claims {
        println!("{:?}", claim);
    }

    println!("[+] The overlapping area is: {} in^2", get_overlap(&claims));
}

fn populate_claims(claims: &mut BTreeMap<i32, Claim>, i_string: &str) {
    let string_vec: Vec<&str> = i_string.split(';').collect();

    for item in string_vec {
        let id_split_vec: Vec<&str> = item.split('@').collect();

        if id_split_vec.len() < 2 {
            continue;
        }

        let claim_id = id_split_vec[0]
            .trim()
            .trim_start_matches('#')
            .parse::<i32>()
            .expect("[!] Claim ID is not a number");
        let claim = claims.entry(claim_id).or_default();

        claim.claim_id = claim_id;

        let lmargin_split_vec: Vec<&str> = id_split_vec[1].split(',').collect();
        claim.left_margin = lmargin_split_vec[0]
            .trim()
            .parse::<i32>()
            .expect("[!] Left margin is not a number");

        let rmargin_split_vec: Vec<&str> = lmargin_split_vec[1].split(':').collect();
        claim.top_margin = rmargin_split_vec[0]
            .trim()
            .parse::<i32>()
            .expect("[!] Top margin is not a number");

        let dimensions_split_vec: Vec<&str> = rmargin_split_vec[1].split('x').collect();
        claim.width = dimensions_split_vec[0]
            .trim()
            .parse::<i32>()
            .expect("[!] Width is not a number");
        claim.height = dimensions_split_vec[1]
            .trim()
            .parse::<i32>()
            .expect("[!] Height is not a number");
    }
}

fn get_overlap(claims: &BTreeMap<i32, Claim>) -> i32 {
    let mut fabric = [[0 as i32; FABRIC_DIMENSION]; FABRIC_DIMENSION];
    let mut overlap = 0;

    for (_, claim) in claims {
        let y_limit = claim.top_margin + claim.height;
        let x_limit = claim.left_margin + claim.width;

        for y in claim.top_margin..y_limit {
            for x in claim.left_margin..x_limit {
                fabric[x as usize][y as usize] += 1;
            }
        }
    }

    for y in 0..FABRIC_DIMENSION as i32 {
        for x in 0..FABRIC_DIMENSION as i32 {
            if fabric[x as usize][y as usize] == 0 {
                print!(".");
                continue;
            }

            overlap +=1;

            if fabric[x as usize][y as usize] == 1 {
                print!("#");

                continue;
            }

            print!("X");
        }
        println!()
    }

    overlap
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_get_overlap() {}
}

// #123 @ 3,2: 5x4; #1 @ 3,2: 5x4; #3 @ 10,6: 15x4

// #1 @ 1,3: 4x4; #2 @ 3,1: 4x4; #3 @ 5,5: 2x2
