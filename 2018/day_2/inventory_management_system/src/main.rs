// SPDX-License-Identifier: MIT

use std::collections::btree_map::BTreeMap;
use std::io;

fn main() {
    let mut user_string = String::new();

    println!(
        "[+] Input box id(s) \n\
         As a comma separated list: abced, adsd, ... \n\
         : "
    );
    io::stdin()
        .read_line(&mut user_string)
        .expect("[!] Failed to read box id list");

    // Given the size isn't statically --compile-time-- known , I need to pass a reference
    println!("[+] The checksum is: {}", calc_checksum(user_string.trim()));
}

// Some magic from the Rust documentation: https://doc.rust-lang.org/std/collections/
fn calc_checksum(i_string: &str) -> i32 {
    let mut num_threes: i32 = 0;
    let mut num_twos:i32 = 0;

    let string_vec: Vec<&str> = i_string.split(',').collect();

    for item in string_vec {
        let new_item = item.trim();
        let prev_twos = num_twos;
        let prev_threes = num_threes;

        let mut count = BTreeMap::new();

        for c in new_item.chars() {
            // Creates a default entry of 0, appending one on occurrence
            *count.entry(c).or_insert(0) += 1;
        }

        for cnt in count.values() {
            if cnt == &2 {
                if prev_twos == num_twos {
                    num_twos += 1;
                }

                continue;
            }

            if cnt == &3 && prev_threes == num_threes {
                num_threes += 1;
            }
        }
    }

    num_threes * num_twos
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn calc_checksum_test() {
        let box_ids = "abcdef, bababc, abbcde, abcccd, aabcdd, abcdee, ababab";

        assert_eq!(12, calc_checksum(&box_ids));
    }
}
